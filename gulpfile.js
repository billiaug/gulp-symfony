var production = false,

    gulp = require('gulp'),
    addsrc = require('gulp-add-src'),
    autoprefixer = require('gulp-autoprefixer'),
    sourcemaps = require('gulp-sourcemaps'),
    replace = require('gulp-replace'),
    concat = require('gulp-concat'),
    del = require('del'),
    eslint = require('gulp-eslint'),
    imagemin = require('gulp-imagemin'),
    imageop = require('gulp-image-optimization'),
    insert = require('gulp-insert'),
    minifycss = require('gulp-minify-css'),
    newer = require('gulp-newer'),
    rename = require('gulp-rename'),
    runsequence = require('run-sequence'),
    sass = require('gulp-ruby-sass'),
    uglify = require('gulp-uglify'),
    strip = require('gulp-strip-comments'),
    watch = require('gulp-watch'),

    config = require('./package.json').config;

gulp.task('scss', function() {
    var task = sass(config.files.scss.src, {
            style: 'expanded',
            sourcemap: true
        })
        //.pipe(changed(config.files.scss.src))
        .pipe(replace('/*!', '/*'))
        .pipe(strip())
        //.pipe(minifycss())
        // .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'));


    if (production) {
        task = task.pipe(minifycss());
    }

    return task.pipe(gulp.dest(config.files.scss.dist));
});

gulp.task('js', function() {
    var task = gulp.src([config.files.js.src + '**.js']) // Vendor does not get linted
        //.pipe(newer(config.files.js.dist + 'scripts.js'))
        //.pipe(eslint({ configFile: '.eslintrc.json' }))
        //.pipe(eslint.formatEach('stylish', process.stderr))
        //.pipe(eslint.failAfterError())
        .pipe(replace('/*!', '/*'))
        .pipe(replace('/**', '/*'))
        .pipe(strip())
        //.pipe(sourcemaps.write())
        //.pipe(addsrc.prepend(config.files.js.src.replace('**', '**/vendor'))) // Still prepend vendor to the concat file
        //.pipe(concat('scripts.js'));

    if (production) {
        task = task.pipe(uglify());
    }

    return task.pipe(insert.prepend('(function($, window,undefined){"use strict";'))
        .pipe(insert.append('})(jQuery, window);'))
        .pipe(gulp.dest(config.files.js.dist));
});

gulp.task('img', function() {
    return gulp.src(config.files.img.src + '**')
        .pipe(newer(config.files.img.dist))
        .pipe(gulp.dest(config.files.img.dist))
        .pipe(imageop({
            optimizationLevel: 5,
            progressive: true,
            interlaced: true
        }))
        .pipe(gulp.dest(config.files.img.dist));
});

gulp.task('watch', function() {
    // Watch scss files
    gulp.watch([config.files.scss.src + '**'], ['scss']);

    // Watch javascript files
    gulp.watch([config.files.js.src + '**'], ['js']);

    // Watch image files
    gulp.watch([config.files.img.src + '**'], ['img']);
});

gulp.task('clean', function(cb) {
    del([config.files.scss.dist, config.files.js.dist, config.files.img.dist], cb)
});

gulp.task('cleanproduction', function(cb) {
    del([config.files.scss.src, config.files.js.src, config.files.img.src], cb)
});

gulp.task('development', function(cb) {
    return runsequence('scss', 'js', 'img', cb);
});

gulp.task('production', ['clean'], function(cb) {
    production = true;
    return runsequence('development', 'cleanproduction', cb);
});

gulp.task('default', ['development'], function() {
    return gulp.start('watch');
});
